# Stage 1: NodeJS and Git installation
FROM golang:latest AS node-git

RUN apt-get update && apt-get install -y curl git nodejs npm

# Stage 2: NPM dependencies installation
FROM node-git AS npm-deps

WORKDIR /usr/src/osf-core

RUN npm install -g @marp-team/marp-core \
    && npm install -g markdown-it-include \
    && npm install -g markdown-it-container \
    && npm install -g markdown-it-attrs

# Stage 3: Go application build
FROM npm-deps AS go-build

WORKDIR /usr/src/osf-core

# Copy the source code
COPY . .

RUN go get golang.org/x/text/transform \
    && go get golang.org/x/text/unicode/norm \
    && go get github.com/cloudflare/circl/dh/x25519@v1.1.0 \
    && go get golang.org/x/tools/go/internal/cgo@v0.7.0 \
    && go get github.com/mattn/go-isatty@v0.0.19 \
    && go install github.com/swaggo/swag/cmd/swag@v1.8.12 \
    && swag init --parseDependency --parseInternal

# Final stage
FROM golang:latest

WORKDIR /usr/src/osf-core

# Copy built Go application and dependencies from previous stages
COPY --from=go-build /usr/src/osf-core .

# Expose ports
EXPOSE 8000/tcp
EXPOSE 443/tcp
EXPOSE 80/tcp

# Launch the API
CMD ["go", "run", "/usr/src/osf-core/main.go"]