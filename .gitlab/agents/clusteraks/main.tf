provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "aks" {
  name     = "cicdprojet"
  location = "West Europe"
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                = "Clusterprojet"
  location            = azurerm_resource_group.aks.location
  resource_group_name = azurerm_resource_group.aks.name
  dns_prefix          = "cicdprojetcluster"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_DS2_v2"
  }

  identity {
    type = "SystemAssigned"
  }
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.aks.kube_config[0]
  sensitive = true

}
